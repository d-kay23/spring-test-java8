package com.example.demo;

import java.lang.management.ManagementFactory;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class HelloController {
    @RequestMapping("/")
    public String index() {
        return "Greetings from Spring Boot! <br>" + ManagementFactory.getRuntimeMXBean().getInputArguments().toString() + "<br> Arguments:" + System.getProperties().toString();
    }

}
